import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import EditarFicha from './components/editarFicha/EditarFicha';
import Home from './components/home/Home';
import ListaFicha from './components/listaFicha/ListaFicha';
import MostrarFicha from './components/mostrarFicha/MostrarFicha';
import NuevaFicha from './components/nuevaFicha/NuevaFicha';
import Login from './components/login/Login';

// Configuración de la rutas del Sitio Web 
const AppRouter = () => {
    return (
        <Router>
            <Switch>
                {/* Páginas */}
                <Route exact path='/' component={Home} />
                <Route path='/login' component={Login} />
                <Route path='/mostrarFicha/editarFicha/:id'>
                    <EditarFicha/>
                </Route>
                <Route path='/fichas/mostrarFicha/:id'>
                    <MostrarFicha/>
                </Route>
                <Route path='/fichas' component={ListaFicha} />
                <Route path='/nuevaFicha' component={NuevaFicha} />
            </Switch>
        </Router>
    )
}

export default AppRouter
