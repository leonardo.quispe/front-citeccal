import React from 'react';
import './../../../assets/styles/divs.css';

class TituloEditar extends React.Component {
    render(){
        return(
            <div id="divTitulo" style={{backgroundColor:'#696666'}} class="px-5 py-1 text-center">
                <h2 class="py-2" style={{ color: "#FFFFFF" }} ><b>Editar Ficha Tecnica</b></h2>
            </div>
        )
    }
}
export default TituloEditar;