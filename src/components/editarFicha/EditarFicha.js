import React from 'react';
import TtituloEditar from './tituloEditar/TtituloEditar';
import FormEditar from './formEditar/FormEditar';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';

class EditarFicha extends React.Component {
    render(){
        return(
            <>
            <MenuLateral />
            <TtituloEditar/> 
			<main role="main" class="">
                <div class="">
				    <FormEditar />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default EditarFicha;