import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faClipboardList, faFilePdf} from "@fortawesome/free-solid-svg-icons";
import './../../../assets/styles/divs.css';
import { Link } from 'react-router-dom';
import ficha from './fichas';

export default function Tabla(){
    const [listFichas, setListFichas] = useState([])
    const [search, setSearch] = useState("")
    const [currentPage, setCurrentPage] = useState(0)

    const obtenerFichas = async () => {
        const res = await ficha.getFichas();
        setListFichas(res)
    }

    const ListFiltered = () => {
        if (search.length > 0 ){
            const filtered = listFichas.filter( ficha => ficha.id.toString().includes(search) )
            
            return filtered.slice(currentPage, currentPage + 5);
        } else {
            return listFichas.slice(currentPage, currentPage + 5)
        }
    }

    const nextPage = () => {
        const cant = listFichas.filter(ficha => ficha.id.toString().includes(search)).length;

        if (cant > currentPage) {
            setCurrentPage(currentPage + 5)
        }
    }

    const prevPage = () => {
        if (currentPage > 0) {
            setCurrentPage(currentPage - 5)
        }
    }

    const buscar = (e) =>{
        setSearch(e.target.value);
        
    }

    useEffect(() => {
        obtenerFichas();
    }, []);

    return(
        <div style={{ backgroundColor:'#A46F1F'}} className="container-fluid vh-100"><br/>
            <div id="divTabla" style={{ backgroundColor:'#E5E5E5'}} class="container px-1 py-5">

                <div class="d-flex container text-center">
                    <div class="p-2 container">
                        <div>
                            <input type="text" class="form-control w-50" onChange={buscar} placeholder="Buscar Fichas Tecnicas"/>
                        </div>
                    </div>
                    <div class="container-fluid ml-auto p-2">
                        <div class="botonNuevo">
                            <Link to="/nuevaFicha" class="btn" style={{backgroundColor:'#696666', color:'#ffffff'}}>Nueva Ficha Tecnica</Link>
                        </div>
                    </div>
                </div><br/>
                <div class="container px-4">
                    <table class="table" id="tablaPag">
                        <thead class="table-dark text-center">
                            <tr>
                                <th scope="col">CODIGO</th>
                                <th scope="col">LINEA</th>
                                <th scope="col">SERIE</th>
                                <th scope="col">ALT. TACO</th>
                                <th scope="col">COLOR</th>
                                <th scope="col">ESTILO</th>
                                <th scope="col">HORMA</th>
                                <th scope="col">PLANTA</th>
                                <th scope="col">OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody class="text-center" style={{backgroundColor:'#FFFFFF'}}>
                            {ListFiltered().map((ficha) => {
                                return(
                                    <tr key={ficha.id}>
                                        <td>{ficha.id}</td>
                                        <td>{ficha.linea}</td>
                                        <td>{ficha.serie}</td>
                                        <td>{ficha.alTaco}</td>
                                        <td>{ficha.color}</td>
                                        <td>{ficha.estilo}</td>
                                        <td>{ficha.codHorma}</td>
                                        <td>{ficha.codPlanta}</td>
                                        <td>
                                            <Link to={`/fichas/mostrarFicha/${ficha.id}`}>
                                                <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                            </Link>
                                            {" "}
                                            <Link to={`/mostrarFicha/editarFicha/${ficha.id}`}>
                                                <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                            </Link>
                                            {" "}
                                            <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                        </td>
                                    </tr>
                                    );      
                                })}
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                            <a className="btn" style={{backgroundColor:'#696666', color: '#FFFFFF'}} onClick={prevPage} >Anterior</a>
                            </li>
                            <li><a style={{color: '#E5E5E5'}}>nume</a></li>
                            <li class="page-item">
                            <a className="btn" style={{backgroundColor:'#696666', color: '#FFFFFF'}} onClick={nextPage} >Siguente</a>
                            </li>
                        </ul>
                    </nav>                                                           
                </div><br/>
            </div>
        </div>  
    )
}
