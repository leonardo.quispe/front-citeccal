import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import logolat from '../../../assets/img/logolat.png';

const MenuLaterar = () => {
    const [ mostrar, setMostrar ] = useState(false);

    const mostrarMenu = () => {
        setMostrar(!mostrar);
    }

    return (
        <div>
            <div id="mostrar-nav" className="body" onClick={mostrarMenu}></div>
                <nav className={`nav-lat ${mostrar && "mostrar"}`}>
                <div id="cerrar-nav" onClick={mostrarMenu}></div>
                <div>
                    <img src={logolat} className="logo"></img>
                </div>
                    <ul className="menu navp">
                        <li><Link to="/" style={{color: '#a46f1f'}}><b>Inicio</b></Link></li>
                        <li><Link to="/fichas" style={{color: '#a46f1f'}}><b>Ficha Tecnica</b></Link></li>
                        <li><Link to="/" style={{color: '#a46f1f'}}><b>Servicios</b></Link></li>
                    </ul>
                </nav>
        </div>
    )
}

export default MenuLaterar
