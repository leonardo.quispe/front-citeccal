import React from 'react';
import Footer from '../acoples/footer/Footer';
import Menu from '../acoples/menu/Menu';
import Inicio from './inicio/Inicio';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';

class Home extends React.Component {
    render(){
        return(
            <>
                <MenuLateral/> 
                <Menu/>
                <main role="main" class="">
                    <Inicio />
                </main>
                <Footer />
	  		</>
        )
    }
}
export default Home;





