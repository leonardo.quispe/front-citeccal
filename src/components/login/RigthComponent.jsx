import React from 'react'
import { useState } from 'react'
import { Redirect } from 'react-router-dom'

const RigthComponent = () => {
    const [ redirect, setRedirect ] = useState(false)

    return (
        <div className="col-6 flex-fill" id="login_rigth">
            <div className="row vh-100 justify-content-center align-items-center">  
                <div className="col-auto" >
                    <h1 className="tit">Website Administrativo</h1><br/>
                    <form onSubmit={() => setRedirect(!redirect)}>
                        <div className="input-group p-2">
                            <input type="text" name="email" placeholder="Usuario" className="form-control" value=""/>
                        </div>
                            
                        <div className="input-group p-2">
                            <input type="password" name="password" placeholder="Contraseña" className="form-control" value=""/>
                        </div><br/>

                        <div className="text-center">
                            <input type="submit" className="btn w-75 rounded-pill bo" value="Iniciar Sesión"/>
                        </div>  
                    </form>
                </div>
            </div>
            {redirect && <Redirect to="/" />}
        </div>
    )
}

export default RigthComponent
