import React from 'react'
import LeftComponent from './LeftComponent'
import '../../assets/styles/login.css';
import RigthComponent from './RigthComponent';

const Login = () => {
    return (
        <div className="row sa" id="login">
            <LeftComponent/>
            <RigthComponent/>
        </div>
    )
}

export default Login
