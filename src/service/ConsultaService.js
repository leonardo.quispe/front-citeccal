import axios from 'axios';

export class ConsultaService {
   
    baseUrl = "http://localhost:8080/api/ficha/";

    getAll(){
        return axios.get(this.baseUrl + "all").then(res => res.data);
    }
    
    save(ficha){
        return axios.post(this.baseUrl + "save", ficha).then(res => res.data);
    }
}